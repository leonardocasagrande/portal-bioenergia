<?php
global $header_type;
//0 = White | 1 = Colored
$header_type = 1;
get_header();

?>
<div class="header-mask"></div>
<main class="page-alunos">
  <div class="container text-center">
    <h1 class="titulo">
      Alunos
    </h1>
    <p class="desc">
      O objetivo do curso de Doutorado em Bioenergia é a formação e capacitação de profissionais e pesquisadores de excelência, dispostos a grandes desafios, inclusive internacionais, e prontos para enfrentar a academia e o mercado de trabalho com conhecimentos profundos de a toda a cadeia da Bioenergia.
      <br><br>
      Conheça os nossos futuros Doutores, seus projetos, áreas de atuação e artigos já publicados.
      <br><br>
      Para conhecer mais sobre o Curso de Bioenergia <a href="<?php get_home_url() ?>/doutorado">clique aqui</a>.
      <br><br>
      Para se tornar um aluno do curso de formação de Doutor em Bioenergia <a href="<?php get_home_url() ?>/wp-content/uploads/2020/03/regulamento_usp.pdf" target="_blank">clique aqui</a>.
      <br><br>
    </p>
  </div>
  <section class="listar-alunos">
    <div class="container">
      <div class="input-group">
        <div class="input-group-prepend">
          <span class="input-group-text" id="basic-addon1"><i class="fas fa-search"></i></span>
        </div>
        <input type="text" class="form-control desabilitar" id="buscar" placeholder="Buscar..." aria-label="Buscar..." aria-describedby="basic-addon1">

      </div>
      <div id="app-alunos" class="row">



      </div>
      <div class="gifload d-none text-center">
        <img class="spin mt-4" src="<?= get_stylesheet_directory_uri(); ?>/dist/img/symbol.png">
      </div>
      <p class="erro d-none">Ocorreu um erro. Por favor contate-nos</p>
      <div class="col-12 text-center">
        <a href="#" id="loadMore" data-paged="1" class="btn-cta">Carregar mais</a> <a href="<?php get_home_url() ?>/nossos-indicadores" class="btn-cta">Nossos Indicadores</a>
      </div>
    </div>
  </section>
</main>

<?php get_footer() ?>