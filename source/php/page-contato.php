<?php

global $header_type;
//0 = White | 1 = Colored
$header_type = 1;

$user = wp_get_current_user();

if ($user->exists()) { // is_user_logged_in() is a wrapper for this line

    $userdata = get_user_meta($user->data->ID);
}

?>
<?php get_header(); ?>
<section class="contato-body">

    <div class="container">

        <div class="row mb-5">

            <div class="col-md-12 text-center color-black-light">

                <h1>Entre em contato</h1>
               
            </div>

        </div>
    </div>
    <div class="fundo-contato">
        <div class="container">
            <div class="row ">

                <div class="col-md-6 enderecos">

                    <div class="col-12 mb-4">
                        <strong>USP – ESALQ – PIRACICABA/SP</strong><br>
                        Secretaria do Programa de Pós-graduação em Bioenergia na USP<br>
                        Alexandre J. Santos<br>
                        <i class="fa fa-comment pr-2" style="font-size:24px"></i> <a target="_blank" href="http://www.esalq.usp.br/pg/programas/bioenergia">http://www.esalq.usp.br/pg/programas/bioenergia</a><br>
                        <i class="fa fa-envelope pr-2" style="font-size:24px"></i> <a href="mailto:pipg.bioen@usp.br">pipg.bioen@usp.br</a><br>
                    </div>

                    <div class="col-12 mb-4">
                        <strong>UNICAMP – CAMPINAS/SP</strong><br>
                        Secretaria do Programa de Pós-graduação em Bioenergia na Unicamp<br>
                        Camila Ferreira Julio/ Andrea Cristina Ferrari
                        <br>
                        <i class='fas fa-phone-volume pr-2' style='font-size:24px'></i>(19) 3521-7592 / 3521-0202
                        <br>
                        <i class="fa fa-envelope pr-2" style="font-size:24px"></i> <a href="mailto:bioenerg@fea.unicamp.br">bioenerg@fea.unicamp.br</a><br>
                    </div>

                    <div class="col-12 mb-4">
                        <strong>UNESP – INSTITUTO DE PESQUISA EM BIOENERGIA – RIO CLARO/SP</strong><br>
                        Secretaria do Programa de Pós-graduação em Bioenergia na UNESP<br>
                        Luís Ricardo M. Oliveira

                        <br>
                        <i class="fa fa-envelope pr-2" style="font-size:24px"></i> <a href="mailto:pipg.bioenergia@ipben.unesp.br">pipg.bioenergia@ipben.unesp.br</a><br>
                    </div>

                </div>

                <div class="col-md-6">
                    <?php
                    $idAuthor = $_GET['authorId'];
                    $emailAuthor = get_userdata($idAuthor);
                    $emailAuthor = $emailAuthor->user_email;
                    // var_dump($emailAuthor);
                    if (!empty($idAuthor)) {
                        echo do_shortcode('[contact-form-7 id="339" title="Formulário de apoiar projeto"]');
                    ?>
                        <script>
                            $(document).ready(function() {
                                $('#orientador-email').val('<?php echo $emailAuthor ?>')
                            })
                        </script>
                    <?php
                    } else {
                        echo do_shortcode('[contact-form-7 id="294" title="Formulário de contato"]');
                    }
                    ?>
                </div>
            </div>

        </div>
    </div>

</section>

<?php get_footer(); ?>