    <footer>

      <div class="container">

        <div class="row py-5">

          <div class="col-12 text-center">

            <img src="<?= get_stylesheet_directory_uri(); ?>/dist/img/logo-bio-energia.png" alt="Logo Portal de Bioenergia" title="Portal de Bioenergia">

          </div>

        </div>

        <div class="row pb-5">

          <div class="col-12">

            <hr>

          </div>

          <div class="col-md-6">

            <div class="col-md-12 pb-2">


              Iniciativa:

            </div>

            <div class="row text-center align-items-center">

              <div class="col-md-4 py-3">

                <img src="<?= get_stylesheet_directory_uri(); ?>/dist/img/logo-unicamp.png" alt="Logo Unicamp" title="Unicamp">

              </div>

              <div class="col-md-4 py-3">

                <img src="<?= get_stylesheet_directory_uri(); ?>/dist/img/logo-usp.png" alt="Logo USP" title="USP">

              </div>

              <div class="col-md-4 py-3">

                <img src="<?= get_stylesheet_directory_uri(); ?>/dist/img/logo-unesp.png" alt="Logo UNESP" title="UNESP">

              </div>

            </div>

          </div>

          <div class="col-md-6">

            <div class="row text-md-right text-center">

              <div class="col-md-12 py-3 social">

                <a href="#" title="Facebook Portal de Bioenergia"><i class="fab fa-facebook-f"></i></a>

                <a href="#" title="Linkedin Portal de Bioenergia"><i class="fab fa-linkedin-in"></i></a>

                <a href="#" title="Instagram Portal de Bioenergia"><i class="fab fa-instagram"></i></a>

              </div>

              <div class="col-md-12 py-3">

                © <?= date('Y') ?> · Todos os direitos resenvador. Portal Bioenergia.

              </div>



            </div>

          </div>

        </div>

      </div>

    </footer>
    <!-- Principal JavaScript do Bootstrap -->

    <?php wp_footer(); ?>

    <script src="https://code.jquery.com/jquery-3.5.1.min.js" integrity="sha256-9/aliU8dGd2tb6OSsuzixeV4y/faTqgFtohetphbbj0=" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.1/dist/umd/popper.min.js" integrity="sha384-9/reFTGAW83EW2RDu2S0VKaIzap3H66lZH81PoYlFhbGU+6BZp6G7niu735Sk7lN" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/js/bootstrap.min.js" integrity="sha384-w1Q4orYjBQndcko6MimVbzY0tgp4pWB4lZ7lr30WKz0vr/aWKhXdBNmNb5D92v7s" crossorigin="anonymous"></script>



    <script src="https://rawgit.com/RobinHerbots/Inputmask/3.x/dist/jquery.inputmask.bundle.js"></script>

    <script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.9.3/Chart.bundle.min.js"></script>

    <script src="https://cdnjs.cloudflare.com/ajax/libs/tiny-slider/2.9.2/min/tiny-slider.js"></script>
    <script src="<?= get_stylesheet_directory_uri(); ?>/dist/js/app.js"></script>
    <script src="https://translate.google.com/translate_a/element.js?cb=googleTranslateElementInit"></script>
    <script>
      function googleTranslateElementInit() {
        new google.translate.TranslateElement({
            pageLanguage: 'pt-BR',
            includedLanguages: 'en'
          },
          'google_translate_element'
        );
      }
    </script>

    </body>

    </html>