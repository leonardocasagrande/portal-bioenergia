<?php

global $header_type;
//0 = White | 1 = Colored
$header_type = 1;

?>
<?php get_header(); ?>
<?php

$user = (isset($_GET['author_name'])) ? get_user_by('slug', $author_name) : get_userdata(intval($author));

if ($user->exists()) {
    $userdata = get_user_meta($user->data->ID);
}
?>
<?php include "perfil-header.php" ?>

<section class="profile-body pb-5" data-idprofile="<?= $user->data->ID; ?> ">

    <div class="container">

        <div class="row py-md-5 pt-3">

            <?php include "perfil-widget.php" ?>

            <?php include "content-users.php" ?>

        </div>


        <?php
        // if (have_posts()) :
        //     while (have_posts()) : the_post();
        //         the_content();
        //     endwhile;
        // else :
        //     echo '<p>Sorry, no posts matched your criteria.</p>';
        // endif;
        ?>

    </div>

</section>



<?php get_footer(); ?>