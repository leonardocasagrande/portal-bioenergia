<?php

if(current_user_can('administrator')){


        $url = get_home_url() . "/wp-admin";

                      header("Location:" .   $url);

                                die();


             }

global $header_type;
//0 = White | 1 = Colored
$header_type = 1;

$user = wp_get_current_user();

if ($user->exists()) { // is_user_logged_in() is a wrapper for this line

    $userdata = get_user_meta($user->data->ID);
}

?>

<?php get_header(); ?>

<?php include "perfil-header.php" ?>

<section class="profile-body pb-5" data-idprofile="<?= $user->data->ID; ?> ">

    <div class="container">

        <div class="row py-md-5 pt-3">

            <?php include "perfil-widget.php" ?>

            <?php include "content-users.php" ?>

        </div>


        </div>

</section>



<?php get_footer(); ?>
