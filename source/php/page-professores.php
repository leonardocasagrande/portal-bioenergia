<?php 
global $header_type;
//0 = White | 1 = Colored
$header_type = 1;
get_header(); 

?>
  <div class="header-mask"></div>
<main class="page-professores">
  <div class="container text-center">
    <h1 class="titulo">
      Professores
    </h1>
    <p class="desc">
    No curso de Doutorado em Bioenergia você encontra Professores especializados e com expertise em todas as linhas de pesquisa do Programa. Conheça alguns desse Professores, alunos que orientam, projetos e disciplinas que ministram no curso.
    <br><br>
    </p>
  </div>
  <section class="listar-professores">
    <div class="container">
      <div class="input-group">
        <div class="input-group-prepend">
          <span class="input-group-text" id="basic-addon1"><i class="fas fa-search"></i></span>
        </div>
        <input type="text" class="form-control desabilitar" id="buscar" placeholder="Buscar..." aria-label="Buscar..." aria-describedby="basic-addon1">
        
      </div>
      <div id="app-professores" class="row">
        
          
      
      </div>
      
      <div class="gifload d-none text-center">
        <img class="spin mt-4" src="<?= get_stylesheet_directory_uri(); ?>/dist/img/symbol.png">
      </div>
      <p class="erro d-none">Ocorreu um erro. Por favor contate-nos</p>
      <div class="col-12 text-center">
          <a href="#" id="loadMore" data-paged="1" class="btn-cta">Carregar mais</a> <a href="<?php get_site_url() ?>/nossos-indicadores" class="btn-cta">Nossos Indicadores</a>
        </div>
    </div>
  </section>
</main>

<?php get_footer() ?>