
<?php 

acf_form_head();

get_header();

?>
<section>
    
    <div class="container" style="margin-top: 13rem;">

    <?php
	
	acf_form(array(
		'post_id'		=> 'new_post',
		'post_title'	=> true,
		'post_content'	=> true,
		'new_post'		=> array(
			'post_type'		=> 'projetos',
			'post_status'	=> 'publish'
		)
	));
	
	?>
        
    </div>

</section>

<?php get_footer(); ?>