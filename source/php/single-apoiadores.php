<?php

global $header_type;
//0 = White | 1 = Colored
$header_type = 1;

$user = wp_get_current_user();

if ( $user->exists() ) { // is_user_logged_in() is a wrapper for this line

    $userdata = get_user_meta( $user->data->ID );
    
}
setPostViews(get_the_ID());
$args=array(
    'post_type' => 'projetos',
    'posts_per_page' => 3,
    'meta_query' => array(
        array(
            'key' => 'apoiadores_wp',
            'value' => get_the_ID(),
            'compare' => '='
        )
    )
        );
        $projetos = new WP_Query($args);
?>
<?php get_header(); ?>
<section class="apoiadores-body">

    <div class="container">

        <div class="row mb-5">

            <div class="col-md-12 text-center">

                <img src="<?= get_field("logomarca");?>" alt="Logo <?= get_the_title(); ?>" title="<?= get_the_title(); ?>">

            </div>

            <div class="col-md-12 descricao"><?= get_field("descricao"); ?></div>            

        </div>

    </div>

</section>
<section class="apoiadores-indicadores">

    <div class="container">

        <div class="row py-5">
            
            <div class="title pr-5 pl-3 pl-md-0">
                Indicadores
            </div>

            <div class="col position-relative">

                <hr>

            </div>            

        </div>
        <div class="row indicadores-lista pb-5 contador">

            <div class="col-md-4 text-center">
                <strong>Recursos aplicados</strong>                
                <span>R$ </span><?= formatNumber(get_field('recursos_aplicados')) ?></span>
                <small>Lorem ipsum dolor sit amet, dolor.</small>
            </div>

            <div class="col-md-4 text-center">
                <strong>Projetos financiados</strong>                
                <span><?php echo formatNumber($projetos->found_posts) ?></span>
                <small>Lorem ipsum dolor sit amet, dolor.</small>

            </div>

            <div class="col-md-4 text-center">
                <strong>Parcerias</strong>                
                <span><?= formatNumber(get_field('parcerias')) ?></span>
                <small>Lorem ipsum dolor sit amet, dolor.</small>

            </div>
        </div>

    </div>

</section>
<section class="projetos-apoiados">

    <div class="container">

        <div class="row py-5">

            <div class="col-md-3 title-column">                
                <strong>Projetos apoiados</strong>
                <span>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed.</span>
                
            </div>
            
            <?php
                

                while($projetos->have_posts()): $projetos->the_post();
            ?>
            <div class="col-md-3">
                <a href="<?php the_permalink() ?>">

                    <img src="<?php the_field('feature_img') ?>" alt="">

                    <div class="col-md-12 pt-2">
                        <strong class="pb-3"><?php the_title() ?></strong>
                        <span>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed</span>
                    </div>
                                                         
                </a>                              
            </div>
                    <?php endwhile; ?>
            
        </div>

    </div>

</section>
<?php 

include "section-apoie-projeto.php";

get_footer();
?>