<?php

global $header_type;
//0 = White | 1 = Colored
$header_type = 1;

$user = wp_get_current_user();

if ($user->exists()) { // is_user_logged_in() is a wrapper for this line
    $userdata = get_user_meta($user->data->ID);

    //echo $userdata['resumo'][0];
    //echo var_dump($userdata);

}



?>


<?php get_header(); ?>

<section>
    <?php //echo do_shortcode('[rev_slider alias="home"]'); ?>
    <div class="banner-home">
    
    </div>
    <div class="container">

        <div class="tabs-sem-sentido">
            <ul class="nav nav-tabs justify-content-between" id="myTab" role="tablist">
                <li class="nav-item">
                    <a class="nav-link active" onclick="loadAny()" id="todos-tab" data-toggle="tab" href="#todos" role="tab" aria-controls="todos" aria-selected="true">todos</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="<?= get_home_url(); ?>/doutorado">O Curso</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" onclick="searchFunction('professores')" id="professores-tab" data-toggle="tab" href="#professores" role="tab" aria-controls="professores" aria-selected="false">professores</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" onclick="searchFunction('alunos')" id="alunos-tab" data-toggle="tab" href="#alunos" role="tab" aria-controls="alunos" aria-selected="false">alunos</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" onclick="loadAny('projetos')" id="projetos-tab" data-toggle="tab" href="#projetos" role="tab" aria-controls="projetos" aria-selected="false">projetos</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" onclick="loadAny('artigos')" id="artigos-tab" data-toggle="tab" href="#artigos" role="tab" aria-controls="artigos" aria-selected="false">artigos</a>
                </li>
            </ul>
            <div class="tab-content" id="myTabContent">
                <div class="tab-pane fade show active" id="todos" role="tabpanel" aria-labelledby="todos-tab">
                    <div class="row" id="app-any">


                    </div>
                    <div class="load-gif d-none text-center">
                        <img class="spin mt-4" src="<?= get_stylesheet_directory_uri(); ?>/dist/img/symbol.png">
                    </div>
                    <div class="text-center mt-4">

                        <button class="load-more py-2 border-0 bg-green rounded text-white px-4 py-1" data-page="1" data-post-type="any" data-load="loadAny" id="load-any">Carregar mais</button>

                    </div>
                </div>

                <div class="tab-pane fade listar-professores text-center" id="professores" role="tabpanel" aria-labelledby="professores-tab">
                    <div id="app-professores" class="row">



                    </div>
                    <div class="load-gif d-none text-center">
                        <img class="spin mt-4" src="<?= get_stylesheet_directory_uri(); ?>/dist/img/symbol.png">
                    </div>
                    <p class="erro d-none">Ocorreu um erro. Por favor contate-nos</p>
                    <a href="<?php echo get_site_url() ?>/professores" class="btn-cta">Ver todos</a>
                </div>
                <div class="tab-pane fade listar-alunos text-center" id="alunos" role="tabpanel" aria-labelledby="alunos-tab">
                    <div id="app-alunos" class="row">
                        <p class="mt-4">O objetivo do curso de Doutorado em Bioenergia é a formação e capacitação de profissionais e pesquisadores de excelência, dispostos a grandes desafios, inclusive internacionais, e prontos para enfrentar a academia e o mercado de trabalho com conhecimentos profundos de a toda a cadeia da Bioenergia.
                            Conheça os nossos futuros Doutores, seus projetos, áreas de atuação e artigos já publicados.
                            Para conhecer mais sobre o Curso de Bioenergia <a href="<?php echo get_site_url() ?>/doutorado">clique aqui</a>
                            Para se tornar um aluno do curso de formação de Doutor em Bioenergia <a href="<?php echo get_site_url() ?>/wp-content/uploads/2020/03/regulamento_usp.pdf" target="_blank">clique aqui</a> </p>


                    </div>

                    <a href="<?php echo get_site_url() ?>/alunos" class="btn-cta">Ver todos</a>

                    <div class="load-gif d-none text-center">
                        <img class="spin mt-4" src="<?= get_stylesheet_directory_uri(); ?>/dist/img/symbol.png">
                    </div>
                    <p class="erro d-none">Ocorreu um erro. Por favor contate-nos</p>
                </div>
                <div class="tab-pane fade" id="projetos" role="tabpanel" aria-labelledby="projetos-tab">
                    <div class="row" id="app-projetos">


                    </div>
                    <div class="load-gif d-none text-center">
                        <img class="spin mt-4" src="<?= get_stylesheet_directory_uri(); ?>/dist/img/symbol.png">
                    </div>
                    <div class="text-center mt-4">

                        <a class="load-more py-2 bg-green rounded text-white px-4 py-1" data-page="1" data-post-type="projetos" id="load-projetos">Carregar mais</a>

                    </div>
                </div>
                <div class="tab-pane fade" id="artigos" role="tabpanel" aria-labelledby="artigos-tab">
                    <div class="row" id="app-artigos">
                        <p class="mt-4">Aqui você encontra artigos elaborados pelos nossos alunos e professores dentro das linhas de pesquisa do curso de formação em Bioenergia. Clique no título para ler o resumo e acessar o artigo completo. </p>

                    </div>
                    <div class="load-gif d-none text-center">
                        <img class="spin mt-4" src="<?= get_stylesheet_directory_uri(); ?>/dist/img/symbol.png">
                    </div>
                    <div class="text-center mt-4">

                        <a class="load-more py-2 bg-green rounded text-white px-4 py-1" data-page="1" data-load="loadAny" data-post-type="artigos" id="load-artigos">Carregar mais</a>

                    </div>
                </div>
            </div>
        </div>


        <div class="row seja-aluno">
            <div class="col-lg-4 img-seja-aluno"></div>
            <div class="col-lg-8 bg-concrete">
                <h2>Seja um aluno</h2>
                <p>Saiba como se tornar um aluno do curso de formação de Doutor em Bioenergia.</p>

                <a href="<?php echo get_site_url() ?>/doutorado" class="btn-cta">Saiba mais</a>
            </div>
        </div>

        <!-- inserir aqui projetos em andamento -->
        <div class="projetos-home">
            <div class="row">
                <div class="col-lg-8">
                    <h2 class="color-green">Projetos em Andamento</h2>
                    <hr>
                    <div id="app-andamento" class="listar-projetos container">

                    </div>
                </div>
                <div class="col-lg-4">
                    <h2 class="color-blue-light">Popular no Portal</h2>
                    <hr>
                    <div id="app-popular" class="listar-popular container">

                    </div>
                    <div class="load-gif d-none text-center">

                        <img class="spin mt-4" src="<?= get_stylesheet_directory_uri(); ?>/dist/img/symbol.png">

                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="contato-home">
        <div class="row m-0">

            <div class="col-lg-6 bg-concrete">
                <h2>Entre em contato</h2>

                <p>Tire suas dúvidas e traga sugestões.</p>

                <a href="<?php get_site_url() ?>/contato" class="btn-cta">Clique aqui</a>
            </div>
            <div class="col-lg-6 img-contato">

            </div>
        </div>
    </div>


    <div class="parceiros-home">
        <h2>Nossos parceiros</h2>
        <div class="container">
            <div class="my-slide" id="carousel-parceiros">
                <?php
                $argsParceiros = array(
                    'post_type' => 'apoiadores',
                    'posts_per_page' => -1
                );
                $parceiros = new WP_Query($argsParceiros);
                if ($parceiros->have_posts()) : while ($parceiros->have_posts()) : $parceiros->the_post();
                ?>
                        <div class="item">
                            <img src="<?= get_field('logomarca') ?>" alt="<?php the_title() ?>">
                        </div>
                <?php endwhile;
                endif; ?>
            </div>
        </div>
    </div>
    <!-- inserir aqui cases de sucesso -->
    <div class="cabecalho bg-bright pt-3 text-center">
        <h2 class="color-black-light font-h1">Casos de sucesso</h2>
        <p class="mb-0">Conheça os casos de sucesso do Doutorado em Bioenergia.</p>
    </div>
    <?php include('successful-cases.php') ?>

    <div class="indicadores-home">

        <div class="container">
            <div class="row">
                <div class="col-md-6 col-lg-8">
                    <h2>Nossos <br> indicadores</h2>
                    <p class="legenda">
                        Conheça todos os indicadores que envolvem o Programa Integrado de Pós-graduação em Bioenergia, como por exemplo, quantidade de alunos, artigos publicados, projetos realizados e muitos outros. <a href="<?php get_site_url() ?>/nossos-indicadores">Veja alguns desses indicadores. </a>
                    </p>
                    <img src="<?= get_stylesheet_directory_uri(); ?>/dist/img/banner-diploma.png" alt="placeholder">
                </div>
                <div class="col-md-6 col-lg-4">
                    <div class="conter-graph mt-4">
                        <canvas id="grafico" height="300"></canvas>
                    </div>
                    <!-- <p class="legenda">
                Lorem ipsum dolor sit amet, dolor sit ame.
                </p> -->
                    <hr>
                    <div class="contador">
                        <div class="numeros">
                            <h3>Recursos captados</h3>
                            <?php echo get_recursos('recursos_capitados', 'projetos') ?>
                            <!-- <p class="legenda">Lorem ipsum dolor sit amet, dolor sit ame.</p> -->
                        </div>
                        <hr>
                        <div class="numeros">
                            <h3>Projetos concluídos</h3>
                            <span class="numero"><?php echo get_projetos_concluidos() ?></span>
                            <!-- <p class="legenda">Lorem ipsum dolor sit amet, dolor sit ame.</p> -->
                        </div>
                    </div>
                </div>
                <div class="col-12">
                    <a href="<?php get_site_url() ?>/nossos-indicadores" class="btn-cta">Saiba mais</a>
                </div>
            </div>
        </div>
    </div>
    <div class="banner-contato">
        <div class="container">
            <div class="row align-items-center">
                <div class="col-lg-2"></div>
                <div class="col-lg-6">
                    <h2>Entre em contato</h2>
                    <p>Tire suas dúvidas e traga sugestões.</p>
                </div>
                <div class="col-lg-4">
                    <a href="<?php get_site_url() ?>/contato" class="btn-cta">Clique aqui</a>
                </div>
            </div>
        </div>
    </div>
</section>


<?php get_footer(); ?>