<section class="apoie-projeto py-3 py-md-5">

    <div class="container">

        <div class="row text-center my-4 text-md-left">
            
            <div class="col-md-7 d-md-flex m-auto">

                <div class="col-md-6">
                    <strong>Se interessou?</strong>
                    Apoie ou solicite um projeto.
                </div>

                <div class="col-md-6 mt-4 mt-md-0 align-items-center d-flex">
                    
                    <a href="<?php get_home_url() ?>projetos" class="btn btn-white m-auto">Ver todos</a>

                </div>

            </div>
            
            

        </div>

    </div>

</section>