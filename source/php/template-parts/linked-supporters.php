<?php

$post = get_post($dataSubfield);

$logo = get_field('logomarca', $dataSubfield);

?>

<a class="author" href="<?= $post->guid ?>">

  <img src="<?= fixAvatar($logo) ?>" alt="<?= $post->post_title; ?>">

  <div class="col">

    <?= $post->post_title; ?>

  </div>

</a>