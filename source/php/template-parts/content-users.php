<?php

$userID = $user->data->ID;

function count_contributions($postType, $fieldName, $status = false)
{

    $today = date('Ymd');

    global $userID;

    $beginDate = array(
        'key' => 'data_inicio',
        'compare' => '<=',
        'value' => $today
    );

    $finalDate  =  array(
        'key' => 'data_final',
        'compare' => '>=',
        'value' => $today
    );

    if ($status) {

        $beginDate = array();

        $finalDate['compare'] = '<=';
    }

    $args = array(


        'post_type' => $postType,

        'posts_per_page' => '-1',

        'meta_query' => array(

            'relation' => 'AND',

            array(
                'key' => $fieldName . '_wp',
                'value' => $userID,
                'compare' => '='
            ),

            $beginDate,
            $finalDate

        )
    );

    $orientadores = new WP_Query($args);


    echo $orientadores->post_count;
}


?>

<div class="col-md-8 my-5 tab-content clearfix">

    <div class="tab-pane active" id="perfil">

        <div class="d-flex justify-content-between">

            <h2 class="color-green">Perfil</h2>

            <?php if (is_user_logged_in() && is_page('perfil')) { ?>

                <a href="<?= get_home_url() ?>/editar-perfil" class="d-flex color-gray-light align-items-center">

                    Editar Perfil<i class="far fa-edit color-green ml-2"></i>

                </a>

                <a href="<?= get_home_url() ?>/wp-admin/" class="d-flex align-items-center color-gray-light">

                    Acessar seus posts <i class="far fa-edit color-green ml-2"></i>

                </a>

            <?php } ?>

            <a href="#" onclick="window.history.back();" class="d-flex px-3 rounded-pill text-white text-center bg-green align-items-center">

                <i class="fas fa-arrow-left text-white mr-2"></i> Voltar

            </a>

        </div>

        <hr class="my-5">

        <h2 class="mt-3  color-black-light"><?= $userdata['first_name'][0]; ?> <?= $userdata['last_name'][0]; ?></h2>

        <h3 class="color-gray-light d-none">Orientador</h3>

        <p class="mt-5"><?= $userdata['resumo'][0]; ?></p>

        <div class="socials my-5">

            <div class="color-gray-light mb-3">

                <i class="fas fa-envelope color-green mr-2"></i> <a class="mailto" href="mailto:<?= $user->user_email; ?>"> <?= $user->user_email; ?> </a>

            </div>

            <div class="color-gray-light">

                <i class="fas fa-phone-alt color-green mr-2"></i> <a class="mailto" href="tel:<?= $userdata['telefone'][0]; ?>"> <?= $userdata['telefone'][0]; ?> </a>

            </div>

        </div>

    </div>

    <div class="tab-pane" id="app-projetos">

        <div class="text-center">

            <button class="load-more" id="load-projetos" data-page="1" data-post-type="projetos" data-load="loadContent">Carregar mais</button>

        </div>

    </div>

    <div class="tab-pane" id="app-artigos">

        <div class="text-center">

            <button class="load-more" id="load-artigos" data-page="1" data-post-type="artigos" data-load="loadContent">Carregar mais</button>

        </div>

    </div>

    <div class="tab-pane" id="indicadores">

        <div class="my-5 indicadores">

            <h2 class="color-green">Indicadores</h2>

            <hr class="my-5">

            <div class="row">

                <div class="col-md-6 item">

                    <div>Produções publicadas</div>
                    <div><?= count_user_posts($userID, 'producoes') ?></div>

                </div>

                <div class="col-md-6 item">

                    <div>

                        Orientações <br>
                        em andamento

                    </div>

                    <div>

                        <?php count_contributions('projetos', 'orientadores') ?>


                    </div>


                </div>


                <div class="col-md-6 item">

                    <div>Orientações <br>
                        concluídas
                    </div>

                    <div> <?php if (count_contributions('projetos', 'orientadores', true)) ?></div>

                </div>

                <div class="col-md-6 item">

                    <div>Projetos em <br>
                        andamento </div>

                    <div> <?php count_contributions('projetos', 'equipe') ?> </div>


                </div>

                <div class="col-md-6 item">

                    <div>Projetos <br>
                        concluídos </div>

                    <div> <?php count_contributions('projetos', 'equipe', true) ?> </div>

                </div>

                <div class="col-md-6 item">

                    <div>Recurso captado </div>


                    <div><?= formatNumber($userdata['recursos_captados'][0]); ?></div>

                </div>

            </div>

        </div>

    </div>

    <div class="tab-pane" id="app-search">

        <div class="text-center">

            <button class="load-more" id="load-artigos" data-page="1" data-post-type="artigos" data-load="loadContent">Carregar mais</button>

        </div>

    </div>

    <div class="load-gif d-none text-center">

        <img class="spin py-4" src="<?= get_stylesheet_directory_uri(); ?>/dist/img/symbol.png">

    </div>

</div>