$(document).ready(function () {
  let secondPath = window.location.pathname;

  let result = secondPath.split("/");
  console.log(result);
  
  switch (result[1]) {
    case "casos-de-sucesso":
      loadCases("cases");

      break;

    case "projetos":
      loadAny("projetos");
      break;

    case "alunos":
      searchFunction(result[1]);

      break;

    case "professores":
      searchFunction(result[1]);

      break;

    case "author":
      loadContent("artigos");

      loadContent("projetos");

      break;

    case "perfil":
      loadContent("artigos");

      loadContent("projetos");

      break;

    case "nossos-indicadores":
      chartPie();

      chartLine();

      break;

    case "doutorado":
      sliderDoutorado();
      console.log("doutorado");
      
      break;

    case "": //Case da home
      loadAny();

      loadCases("cases");

      sliderParceiros();

      chartPie();

      loadProjetosAndamento();

      loadPopulares();

      break;
  }
});
