var url = document.location.origin;

var loadMore = $(".load-more");

function loadButton(dataPost, loadType) {
  var paged = parseInt($("#load-" + dataPost).attr("data-page")) + 1;

  var loadRef = $("#load-" + dataPost);

  loadRef.attr("data-page", paged);

  loadRef.attr("disabled", "disabled");

  loadType(dataPost, paged);
}

function fixAvatarUrl(string) {
  var res = string.replace(/-avatar/, "");

  return res;
}

loadMore.click(function () {
  var dataPost = $(this).data("postType");

  const loadFunc = window[$(this).data("load")];

  loadButton(dataPost, loadFunc);
});

$(document).ready(function () {
  /*   $("#telefone_407, #telefone_11, #telefone_33").inputmask({
      mask: ["(99) 9999-9999", "(99) 99999-9999", "+99 (99) "],
      keepStatic: true
    }); */

  var max_chars = 15;

  $("#telefone_407, #telefone_11, #telefone_33").keydown(function (e) {
    if ($(this).val().length >= max_chars) {
      $(this).val($(this).val().substr(0, max_chars));
    }
  });

  $("#telefone_407, #telefone_11, #telefone_33").keyup(function (e) {
    if ($(this).val().length >= max_chars) {
      $(this).val($(this).val().substr(0, max_chars));
    }
  });

  $("#acf-field_5df3db87a3ab6").maskMoney({
    prefix: "R$ ",
    decimal: ",",
    thousands: ".",
  });
});

let formatter = new Intl.NumberFormat([], {
  style: "currency",
  currency: "BRL",
});

var value = $(".value").text();

$(".value").html(formatter.format(value));

function trimString(content, size) {
  if (content.length > size) {
    return content.substring(0, size) + "...";
  }

  return content;
}

function inViewport($ele) {
  var lBound = $(window).scrollTop(),
    uBound = lBound + $(window).height(),
    top = $ele.offset().top,
    bottom = top + $ele.outerHeight(true);

  return (
    (top > lBound && top < uBound) ||
    (bottom > lBound && bottom < uBound) ||
    (lBound >= top && lBound <= bottom) ||
    (uBound >= top && uBound <= bottom)
  );
}
var countVar = 0;
$(window).on("scroll", function () {
  var top = $(this).scrollTop();

  /* CONTADOR */
  if (
    $(".contador").length > 0 &&
    inViewport($(".contador")) &&
    countVar == 0
  ) {
    countVar++;
    $(".numero").each(function () {
      if ($(this).text().indexOf(".") !== -1) {
        $(this)
          .prop("Counter", 0)
          .animate(
            {
              Counter: $(this).text(),
            },
            {
              duration: 4000,
              easing: "swing",
              step: function (now) {
                $(this).text(Math.abs(now).toFixed(1));
              },
            }
          );
      } else {
        $(this)
          .prop("Counter", 0)
          .animate(
            {
              Counter: $(this).text(),
            },
            {
              duration: 4000,
              easing: "swing",
              step: function (now) {
                $(this).text(Math.ceil(now));
              },
            }
          );
      }
    });
  }
});

$('a[data-toggle="tab"]').on("shown.bs.tab", function (e) {
  e.relatedTarget.removeAttribute("onclick"); // previous active tab
});

function noTilde(string) {
  if (string.normalize != undefined) {
    string = string.normalize("NFKD");
  }
  return string.replace(/[\u0300-\u036F]/g, "");
}
