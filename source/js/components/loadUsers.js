//Search function
function searchFunction(typeUser) {
  function loadUser(paged) {
    var local = $("#app");

    var search = $("#buscar").val();

    if (search == undefined) {
      search = "";
    }
    switch (typeUser) {
      case "alunos":
        local = $("#app-alunos");
        break;
      case "professores":
        local = $("#app-professores");
    }
    $.ajax({
      url: `${url}/wp-json/api-portalbio/v1/users/${typeUser}/${paged}/${noTilde(
        search
      )}`,
      beforeSend: function () {
        $(".load-gif").removeClass("d-none");
        $(".desabilitar").attr("disabled");
      },
      statusCode: {
        200: function (data) {
          var dados = data;
          if (dados.length) {
            if (search != null && paged == 1) {
              local.html("");
            }

            dados.forEach(function (dado) {
              var former = "";

              if (dado.ex_aluno.length > 1) {
                former = "Ex-aluno";
              }

              local.append(
                '<div class="col-lg-4 p-3"><div class="card"><div class="card-header"><div class="box-avatar" style="background: url(' +
                  fixAvatarUrl(dado.img_url) +
                  ') center center no-repeat" title="' +
                  dado.nome +
                  '"></div><div class=" card-body pb-0"><h2 class="mb-3"><a class="color-black-light" href="' +
                  url +
                  "/author/" +
                  dado.link +
                  '">' +
                  dado.nome +
                  "</a></h2><div> " +
                  former +
                  '<div class="text-uppercase">' +
                  dado.instituto +
                  '</div></div></div><div class="card-footer"><ul><li><i class="fas fa-envelope"></i> <a href="mailto:' +
                  dado.email +
                  '">' +
                  dado.email +
                  '</a></li><li><i class="fas fa-phone-alt"></i> <a href="tel:' +
                  dado.telephone +
                  '">' +
                  dado.telephone +
                  "</a></li></ul></div></div></div></div>"
              );
            });
          } else {
            $("#loadMore").remove();
          }
        },
        404: function () {
          $(".erro").removeClass("d-none");
        },
        500: function () {
          $(".erro").removeClass("d-none");
        },
      },
    }).always(function () {
      $(".desabilitar").removeAttr("disabled");
      $(".load-gif").addClass("d-none");
    });
  }

  loadUser(1);

  $("#loadMore").on("click", function (e) {
    e.preventDefault();

    var paged = parseInt($("#loadMore").attr("data-paged")) + 1;

    $("#loadMore").attr("data-paged", paged);

    loadUser(paged);
  });

  $("#basic-addon1").on("click", function (e) {
    $("#app-alunos").html("");

    $("#app-professores").html("");

    loadUser(1);
  });

  $("#buscar").on("keypress", function (e) {
    if (e.which == 13) {
      loadUser(1);
    }
  });
}
