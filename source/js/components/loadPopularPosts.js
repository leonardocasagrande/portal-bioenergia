function loadPopulares(type = 'post') {
    $.ajax({

        url: `${url}/wp-json/api-portalbio/v1/popular-post/${type}`,

        type: "GET",


        beforeSend: function () {
            $('.load-gif').removeClass('d-none');
        },

        dataType: "json",

        statusCode: {

            200: function (data) {



                var content = `
  
      ${data.map((element, index) => ` 
  
      <div class="popular row mb-4">
          <div class="col-lg-2">
            <span class="numero">0${index + 1}</span>
          </div>
          <div class="col-lg-10">
            <h3 class="mb-4 mb-lg-0"><a href="${element.permalink}">${element.titulo}</a></h3>
            <div class="autor">
              <img src="${fixAvatarUrl(element.imagem_autor)}" alt="${element.nome_autor}">
              <div class="text-capitalize">
              ${element.nome_autor}<br>
              ${element.data}
              </div>
            </div>
          </div>
        </div>`).join('')}`;

                $("#app-popular").append(content);

                // if (paged == numPosts) {

                //   $('#load-' + postType).remove();

                //   return;

                // }

            },
            404: function () {
                alert("Ops, houve algum erro")
            },
            500: function () {
                alert("Ops, houve algum erro")
            }
        }
    }).always(function () {
        $('.load-gif').addClass('d-none');
    })
}