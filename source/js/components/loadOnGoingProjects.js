function loadProjetosAndamento() {
    $.ajax({

        url: `${url}/wp-json/api-portalbio/v1/ongoing-post`,

        type: "GET",



        beforeSend: function () {
            $('.load-gif').removeClass('d-none');
        },

        dataType: "json",

        statusCode: {

            200: function (data) {



                var content = `
  
      ${data.map(element => ` 
  
      <div class="projeto row">
      <div class="col-lg-4 mb-4 mb-lg-0 img-projeto" style="background-image:url(${element.imagem_projeto})">
        
      </div>
      <div class="col-lg-8">
       <div class="cabecalho">
       <h3><a href="${element.permalink}">${element.titulo}</a></h3>
        <p>${element.resumo}</p>
       </div>
       <div class="rodape">
         <div class="autor mb-4 mb-lg-0">
          <img class="foto-autor" src="${fixAvatarUrl(element.autor.imagem_autor)}" alt="${element.autor.nome}">
          <div class="text-capitalize ">
           ${element.autor.nome}<br>
           ${element.data_final}
          </div>
         </div>
         <a href="/contato?authorId=${element.autor.id}" class="btn-cta">Apoiar</a>
        </div>
      </div>
    </div>`).join('')}`;

                $("#app-andamento").append(content);

                // if (paged == numPosts) {

                //   $('#load-' + postType).remove();

                //   return;

                // }

            },
            404: function () {
                alert("Ops, houve algum erro")
            },
            500: function () {
                alert("Ops, houve algum erro")
            }
        }
    }).always(function () {
        $('.load-gif').addClass('d-none');
    })
}