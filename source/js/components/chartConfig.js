
function chartPie() {
    var dataPie = [];
    var dataLine = [];
    $.ajax({
        url: `${url}/wp-json/api-portalbio/v1/charts`,

        type: "GET",
        dataType: "json",

        statusCode: {
            200: function (data) {
                dataPie = data.pie;
                var ctx = document.getElementById('grafico').getContext('2d');
                var chart = new Chart(ctx, {
                    // The type of chart we want to create
                    type: 'pie',

                    // The data for our dataset
                    data: {
                        labels: ['Projetos concluidos', 'Projetos em andamento'],
                        datasets: [{
                            backgroundColor: ['rgb(242, 153, 74)', 'rgb(13, 177, 214)'],
                            data: dataPie
                        }]
                    },

                    // Configuration options go here
                    options: {
                        responsive: true,
                        legend: {
                            position: 'bottom',
                            align: 'start'
                        },
                        tooltips: false,
                        animation: {
                            duration: 500,
                            easing: "easeOutQuart",
                            onProgress: function () {
                                var ctx = this.chart.ctx;
                                ctx.font = Chart.helpers.fontString(Chart.defaults.global.defaultFontFamily, 'normal', Chart.defaults.global.defaultFontFamily);
                                ctx.textAlign = 'center';
                                ctx.textBaseline = 'bottom';

                                this.data.datasets.forEach(function (dataset) {

                                    for (var i = 0; i < dataset.data.length; i++) {
                                        var model = dataset._meta[Object.keys(dataset._meta)[0]].data[i]._model,
                                            total = dataset._meta[Object.keys(dataset._meta)[0]].total,
                                            mid_radius = model.innerRadius + (model.outerRadius - model.innerRadius) / 2,
                                            start_angle = model.startAngle,
                                            end_angle = model.endAngle,
                                            mid_angle = start_angle + (end_angle - start_angle) / 2;

                                        var x = mid_radius * Math.cos(mid_angle);
                                        var y = mid_radius * Math.sin(mid_angle);

                                        ctx.fillStyle = '#fff';
                                        ctx.font = '100 50px Roboto'
                                        if (i == 3) { // Darker text color for lighter background
                                            ctx.fillStyle = '#444';
                                        }
                                        // var percent = String(Math.round(dataset.data[i] / total * 100)) + "%";
                                        ctx.fillText(dataset.data[i], model.x + x, model.y + y);
                                        // Display percent in another line, line break doesn't work for fillText
                                        // ctx.fillText(percent, model.x + x, model.y + y + 15);
                                    }
                                });
                            }
                        }
                    }
                });

            }
        }
    })

}
function chartLine() {
    var ctx2 = document.getElementById('linhas').getContext('2d');
    var chart2 = new Chart(ctx2, {
        // The type of chart we want to create
        type: 'line',

        // The data for our dataset
        data: {
            labels: ['8', '9', '10', '11', '12'],
            datasets: [{
                backgroundColor: 'rgba(0, 0, 0,0)',
                borderColor: 'rgb(0,0,0)',
                pointBackgroundColor: 'rgb(0,0,0)',
                pointHoverBackgroundColor: 'rgb(0,0,0)',
                pointHoverBorderColor: 'rgb(0,0,0)',
                pointRadius: 5,
                pointHoverRadius: 7,
                borderWidth: 5,
                data: [15000, 17000, 16500, 20000, 28000]
            }]
        },

        // Configuration options go here
        options: {
            responsive: true,
            legend: false,
            tooltips: false
        }
    });
}