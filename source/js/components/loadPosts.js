const loadContent = function (postType, paged = 1, search, perPage = 2) {
  var  postTypeSearch = postType;
  if(postTypeSearch == 'artigos'){
    postTypeSearch = 'producoes'
  }
    const appType = $("#app-" + postType);

    var author = $('.profile-body').data('idprofile');

    if (search) {

        appType.html('')

        // && perPage = -1;

    }

    $.ajax({

        url: `${url}/wp-json/wp/v2/${postTypeSearch}`,

        type: "GET",

        data: {

            per_page: 2,

            search: search,

            page: paged,

            author: author,

            _fields: ['fimg_url', 'title', 'acf', 'link', 'author', 'excerpt']

        },

        beforeSend: function () {
            $('.load-gif').removeClass('d-none');

        },

        dataType: "json",

        statusCode: {

            200: function (data, status, xhr) {

                const numPosts = xhr.getResponseHeader('X-WP-TotalPages');

                var content;

                if (data.length) {

                    content = `
    
        ${data.map(element => ` 
    
          <div class="item shadow mb-5 bg-white w-100">
    
           <div class="img-case" style="background: url(${element.acf.feature_img})">
    
          </div>
    
          <div class="content p-4 ">

          <div class="box">
      
            <h2 class="my-2">${trimString(element.title.rendered, 40)}</h2>
              
            ${trimString(element.excerpt.rendered, 200)}
    
           <div>
    
            <a href="${element.link}" target="_blank">Ler mais</a>
    
            </div>
    
            </div>
    
          </div>
    
        </div>`).join('')}`;

                } else {
                    if(postType == "producoes"){
                        content = `Não há produções intelectuais publicadas`;
                    }
                    content = `Não há ${postType} publicados`;
                    $('#load-' + postType).remove();
                }

                appType.append(content);

                if (paged >= numPosts) {

                    $('#load-' + postType).remove();
                }
            },
            404: function () {
                alert("Ops, houve algum erro");

                $('#load-' + postType).remove();
            },
            500: function () {
                alert("Ops, houve algum erro");

                $('#load-' + postType).remove();
            }
        }
    }).always(function () {
        $('.load-gif').addClass('d-none');
        loadMore.removeAttr("disabled", 'disabled');
    })
};




