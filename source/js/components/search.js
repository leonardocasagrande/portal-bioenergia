const searchTab = $(".search-tab");

const searchBtn = $(".search-btn");

$(".nav-men").click(function () {
  $("#form-search").removeClass("d-flex");

  setTimeout(function () {
    if (searchTab.hasClass("active")) {
      $("#form-search").addClass("d-flex");
    }
  }, 10);
});

searchTab.click(function () {
  const search = $(this).data("searchRef");

  searchBtn.attr("data-type-custom", search);
});

searchBtn.click(function () {
  const search = $(this).data("search");

  const type = document.getElementsByClassName("search-btn")[0].dataset
    .typeCustom;

  const searchInput = $("#search-" + search).val();

  if (!searchInput) return alert("Campo de pesquisa está vázio");

  const loadFunc = window[$(this).data("load")];

  loadFunc(type, 1, searchInput);
});
