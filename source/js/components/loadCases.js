const loadCases = function (postType, paged = 1) {

    $.ajax({

        url: `${url}/wp-json/wp/v2/casos`,

        type: "GET",

        data: {

            per_page: 4,

            page: paged,

            _fields: ['title', 'acf', 'content', 'link', 'excerpt']

        },

        beforeSend: function () {
            $('.load-gif').removeClass('d-none');
        },

        dataType: "json",

        statusCode: {

            200: function (data, status, xhr) {

                const numPosts = xhr.getResponseHeader('X-WP-TotalPages');

                var content = `
      
          ${data.map(element => ` 
      
            <div class="item shadow">
      
             <div class="img-case" style="background: url(${element.acf.imagem})">
      
            </div>
      
            <div class="content">
      
            <div class="box">
      
              <span class="color-red">CASES</span>
      
              <h2 class="my-2">${trimString(element.title.rendered, 40)}</h2>
                
              ${trimString(element.excerpt.rendered, 200)}
      
             <div>
      
              <a href="${element.link}" target="_blank">Ler mais</a>
      
              </div>
      
              </div>
      
            </div>
      
          </div>`).join('')}`;

                $("#app-" + postType).append(content);

                if (paged == numPosts) {

                    $('#load-' + postType).remove();
                }

            },
            404: function () {
                alert("Ops, houve algum erro")
            },
            500: function () {
                alert("Ops, houve algum erro")
            }
        }
    }).always(function () {
        $('.load-gif').addClass('d-none');
    })
}


