function sliderParceiros() {

    var sliderParceiros = tns({
        container: '#carousel-parceiros',
        items: 1,
        slideBy: 'page',
        controls: false,
        gutter: 20,
        navPosition: 'bottom',
        autoplay: true,
        autoplayButtonOutput: false,
        preventScrollOnTouch: 'force',
        responsive: {
            768: {
                items: 2
            },
            1024: {
                items: 4
            }
        }
    });
}


function sliderDoutorado() {
    var slider = tns({
        container: '#carousel-editais',
        items: 1,
        slideBy: 'page',
        controls: false,
        gutter: 20,
        navPosition: 'bottom',
        autoplay: true,
        autoplayButtonOutput: false,
        responsive: {
            768: {
                items: 3
            }
        }
    });
}
