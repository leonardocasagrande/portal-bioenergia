const loadAny = function (postType = "any", paged = 1) {
  var postTypeSearch = postType;
  if(postTypeSearch == 'artigos'){
    postTypeSearch = 'producoes'
  }
  $.ajax({
    url: `${url}/wp-json/api-portalbio/v1/all-posts?paged=${paged}&type=${postTypeSearch}`,

    type: "GET",

    beforeSend: function () {
      $(".load-gif").removeClass("d-none");
    },

    dataType: "json",

    statusCode: {
      200: function (data, status, xhr) {
        const numPosts = xhr.getResponseHeader("X-WP-TotalPages");

        var content = `
    
        ${data
          .map(
            (element) => ` 
    
        <a href="${element.link}" class="bordado mb-4">
        <div class="artigo">
            <div class="cabecalho">
                <div class="img-projeto" style="background-image:url(${
                  element.featured_img_src
                })"></div>
            <h2 class="my-4">${element.title}</h2>
            <p class="resumo">${trimString(element.excerpt, 250)}</p>
            </div>

            <div class="autor">
                <div class="img-autor" style="background: url(${fixAvatarUrl(
                  element.author.imagem_autor
                )}) center center no-repeat" title="${
              element.author.nome
            }"> </div>
                <div class="text">
                ${element.author.nome}<br>
                ${element.post_date}
                </div>
            </div>
            <div class="interacoes mt-2">
                <i class="fas fa-comment"></i> ${
                  element.comments_number
                } comentarios <i class="far fa-eye"></i> ${
              element.post_views_count
            } Visualizações
            </div>
        </div>
    </a>`
          )
          .join("")}`;

        $("#app-" + postType).append(content);

        if (paged == numPosts) {
          $("#load-" + postType).remove();
        }
      },
      404: function () {
        alert("Ops, houve algum erro");
      },
      500: function () {
        alert("Ops, houve algum erro");
      },
    },
  }).always(function () {
    $(".load-gif").addClass("d-none");
  });
};
