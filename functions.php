<?php


function fixAvatar($string){


    $avatar = str_ireplace('-avatar', '', $string);


    return $avatar;
 }

/* 
https://wordpress.stackexchange.com/questions/5275/author-url-rewrite
function add_rewrite_rules($rules) {
    $newrules['author/([^/]+)/songs/?$'] = 'index.php?post_type=songs&author=$matches[1]';
    $newrules['author/([^/]+)/songs/page/?([0-9]{1,})/?$'] = 'index.php?post_type=songs&locations=$matches[1]&paged=$matches[2]';

    $rules = $newrules + $rules;
    return $rules;
}

function flushRules() {
global $wp_rewrite;
$wp_rewrite->flush_rules();
}
add_filter('rewrite_rules_array', 'add_rewrite_rules');

/* This function should only really be run once per change of rules - comment out 
add_filter('init','flushRules'); */

/* Disable WordPress Admin Bar for all users but admins. */

 

show_admin_bar(false);

add_theme_support('post-thumbnails');
function tn_custom_excerpt_length( $length ) {
    return 35;
    }
    add_filter( 'excerpt_length', 'tn_custom_excerpt_length', 999 );
$user = wp_get_current_user();

function replace_core_jquery_version()
{
    wp_deregister_script('jquery');
    // Change the URL if you want to load a local copy of jQuery from your own server.
    wp_register_script('jquery', "https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js", array(), '3.4.1');
}
add_action('wp_enqueue_scripts', 'replace_core_jquery_version');


$user = wp_get_current_user();


function create_post_type()
{

    
    /**
     * Projetos
     */
    register_post_type(
        'projetos',
        array(
            'labels' => array(
                'name' => __('Projetos'),
                'singular_name' => __('Projetos')
            ),
            'public' => true,
            'has_archive' => false,
            'show_in_rest' => true,
            'menu_icon' => 'dashicons-clipboard',
            'supports' => array('title', 'editor', 'author', 'excerpt')

        )
    );

    /**
     * Artigos
     */
    register_post_type(
        'producoes',
        array(
            'labels' => array(
                'name' => __('Produções'),
                'singular_name' => __('Produção')
            ),
            'public' => true,
         
            'has_archive' => false,
            'menu_icon' => 'dashicons-text-page',
            'show_in_rest' => true,
            'supports' => array('title', 'editor', 'excerpt', 'comments')
        )
    );

    /**
     * Apoiadores
     */

    register_post_type(
        'apoiadores',
        array(
            'labels' => array(
                'name' => __('Apoiadores'),
                'singular_name' => __('Apoiadores')
            ),
            'public' => true,
            'has_archive' => false,
            'show_in_rest' => true,
            'menu_icon' => 'dashicons-buddicons-buddypress-logo',


        )
    );
    /**
     * Casos de sucesso
     */

    register_post_type(
        'casos',
        array(
            'labels' => array(
                'name' => __('Casos de sucesso'),
                'singular_name' => __('Casos de sucesso')
            ),
            'public' => true,
            'has_archive' => false,
            'show_in_rest' => true,
            'supports' => array('title', 'editor', 'author', 'thumbnail', 'excerpt')
        )
    );
}

add_action('init', 'create_post_type');

//hook into the init action and call create_book_taxonomies when it fires
//add_action('init', 'create_topics_hierarchical_taxonomy', 0);

//create a custom taxonomy name it topics for your posts

 function create_topics_hierarchical_taxonomy()
 {

     register_taxonomy('categorias', array('producoes'), array(
         'hierarchical' => true,
        'show_ui' => true,
        'show_admin_column' => true,
       'query_var' => true,
       'show_in_rest' => true
       
    ));
 }

function apoiadores_remove_menu_items()
{

    $user = wp_get_current_user();

    $allowed_roles = array('contributor');

    if (array_intersect($allowed_roles, $user->roles)) {

        remove_menu_page('edit.php?post_type=apoiadores');
    }

    $allowed_roles = array('editor');

    if (array_intersect($allowed_roles, $user->roles)) {

        remove_menu_page('edit.php?post_type=page');

        remove_menu_page('admin.php?page=wpcf7');

        remove_menu_page('tools.php');

    }

}

add_action('admin_menu', 'apoiadores_remove_menu_items');


/**
 * Logic to populate the orientador select in acf
 */
function select_orientador_choices($field)
{

    $value = $field['value'];

    $choices = $field['choices'];

    $args = array(
        'role' => 'editor'
    );

    $users = get_users($args);

    foreach ($users as $index => $user) {

        $user_id = $users[$index]->ID;

        $field['choices'][$user_id] = $users[$index]->first_name . " (" . $users[$index]->user_nicename . ")";
    }

    // Restore original post data.
    wp_reset_postdata();

    // The current value of the field is required to be in the list!
    // Also avoids adding it as a duplicate
    if (!empty($value) && array_key_exists($value, $choices) && !array_key_exists($value, $field['choices'])) {

        $field['choices'][$value] = $choices[$value];
    }

    return $field;
}

add_filter('acf/prepare_field/name=orientador', 'select_orientador_choices');

/**
 * Logic to populate the Alunos select in acf
 */
function select_alunos_choices($field)
{

    $value = $field['value'];

    $choices = $field['choices'];

    $args = array(
        'role' => 'contributor'
    );

    $users = get_users($args);

    foreach ($users as $index => $user) {

        $user_id = $users[$index]->ID;

        $field['choices'][$user_id] = $users[$index]->first_name . " (" . $users[$index]->user_nicename . ")";
    }

    $args = array(
        'role' => 'editor'
    );

    $users = get_users($args);

    foreach ($users as $index => $user) {

        $user_id = $users[$index]->ID;

        $field['choices'][$user_id] = $users[$index]->first_name . " (" . $users[$index]->user_nicename . ")";
    }

    // Restore original post data.
    wp_reset_postdata();

    // The current value of the field is required to be in the list!
    // Also avoids adding it as a duplicate
    if (!empty($value) && array_key_exists($value, $choices) && !array_key_exists($value, $field['choices'])) {

        $field['choices'][$value] = $choices[$value];
    }

    return $field;
}

add_filter('acf/prepare_field/name=aluno', 'select_alunos_choices');

/**
 * Logic to populate the apoiadores select in acf
 */
function select_apoiadores_choices($field)
{

    $value = $field['value'];

    $choices = $field['choices'];

    $args = array(
        'post_type' => 'apoiadores'
    );

    $query = new WP_Query($args);

    if ($query->have_posts()) {

        while ($query->have_posts()) {

            $query->the_post();

            $apoiador_id = get_the_ID();

            $apoiador_name = get_the_title();

            $field['choices'][$apoiador_id] = $apoiador_name;
        }
    }

    // Restore original post data.
    wp_reset_postdata();

    // The current value of the field is required to be in the list!
    // Also avoids adding it as a duplicate
    if (!empty($value) && array_key_exists($value, $choices) && !array_key_exists($value, $field['choices'])) {

        $field['choices'][$value] = $choices[$value];
    }


    return $field;
}

add_filter('acf/prepare_field/name=apoiador', 'select_apoiadores_choices');


add_filter('acf/rest_api/field_settings/show_in_rest', '__return_true');


add_filter('acf/save_post', 'convert_apoiadores_wp_meta', 20);

add_filter('acf/save_post', 'convert_equipe_wp_meta', 20);

add_filter('acf/save_post', 'convert_orientadores_wp_meta', 20);


function convert_apoiadores_wp_meta($post_id)
{

    $meta_key = 'apoiadores_wp';

    delete_post_meta($post_id, $meta_key);

    $saved_values = array();

    if (have_rows('apoiadores', $post_id)) {
        while (have_rows('apoiadores', $post_id)) {
            the_row();


            $apoiador = get_sub_field('apoiador');


            if (isset($saved_values[$apoiador])) {

                continue;
            }


            add_post_meta($post_id, $meta_key, $apoiador, false);


            $saved_values[$apoiador] = $apoiador;
        }
    }
}


function convert_equipe_wp_meta($post_id)
{

    $meta_key = 'equipe_wp';

    delete_post_meta($post_id, $meta_key);

    $saved_values = array();

    if (have_rows('equipe', $post_id)) {
        while (have_rows('equipe', $post_id)) {
            the_row();


            $aluno = get_sub_field('aluno');


            if (isset($saved_values[$aluno])) {

                continue;
            }


            add_post_meta($post_id, $meta_key, $aluno, false);


            $saved_values[$aluno] = $aluno;
        }
    }
}


function convert_orientadores_wp_meta($post_id)
{

    $meta_key = 'orientadores_wp';

    delete_post_meta($post_id, $meta_key);

    $saved_values = array();

    if (have_rows('orientadores', $post_id)) {
        while (have_rows('orientadores', $post_id)) {
            the_row();


            $orientador = get_sub_field('orientador');


            if (isset($saved_values[$orientador])) {

                continue;
            }


            add_post_meta($post_id, $meta_key, $orientador, false);


            $saved_values[$orientador] = $orientador;
        }
    }
}


function linked_users($field, $subField, $componentUser, $postid, $infoUser)
{

    if (have_rows($field, $postid)) {

        while (have_rows($field, $postid)) {

            the_row();

            $dataSubfield = get_sub_field($subField);

            if ($infoUser == true) {

                $data = get_userdata($dataSubfield);

                $avatar = get_avatar_url($dataSubfield);

                $avatar = fixAvatar($avatar);
            }

            include($componentUser);
        }
    } else {

        echo '';
    }
}

//contar artigos publicados
function artigosPubli()
{
    $count_posts = wp_count_posts('artigos');

    return $published_posts = $count_posts->publish;
}

function getPostViews($postID)
{
    $count_key = 'post_views_count';
    $count = get_post_meta($postID, $count_key, true);
    if ($count == null) {
        delete_post_meta($postID, $count_key);
        add_post_meta($postID, $count_key, '0');
        return null;
    }
    return intval($count);
}

function setPostViews($postID)
{
    $count_key = 'post_views_count';
    $count = get_post_meta($postID, $count_key, true);
    if ($count == '') {
        $count = 0;
        delete_post_meta($postID, $count_key);
        add_post_meta($postID, $count_key, '0');
    } else {
        $count++;
        update_post_meta($postID, $count_key, $count);
    }
}

// Remove issues with prefetching adding extra views
remove_action('wp_head', 'adjacent_posts_rel_link_wp_head', 10, 0);

function get_json_popular_posts($request)
{
    $type = $request['type'];

    $argsPopulares = array(
        'post_type' => $type,
        'orderby' => 'meta_value_num',
        'meta_key' => 'post_views_count',
        'post_per_page' => 4
    );
    $postsPopulares = new WP_Query($argsPopulares);
    $postsPopularesJSON = array();
    $postPopular = array();
    while ($postsPopulares->have_posts()) {
        $postsPopulares->the_post();
        $postPopular['titulo'] = get_the_title();
        $postPopular['nome_autor'] = get_the_author_meta('display_name', $postsPopulares->post_author);
        $postPopular['imagem_autor'] = get_avatar_url($user->ID, array('size' => 50));
        $postPopular['data'] = get_the_date('M, d Y');
        $postPopular['permalink'] = get_the_permalink();
        array_push($postsPopularesJSON, $postPopular);
        // var_dump($postsPopularesJSON);
    }
    // $postsPopularesJSON = json_encode($postsPopularesJSON);
    return $postsPopularesJSON;
}

add_action('rest_api_init', function () {
    register_rest_route('api-portalbio/v1', '/popular-post/(?P<type>[-\w]+)', array(
        'methods' => 'GET',
        'callback' => 'get_json_popular_posts',
    ));
});



function get_json_ongoing_posts($request)
{
    $today = date('Ymd');

    $argsInicial = array(
        'post_type' => 'projetos',
        'posts_per_page' => 3,
        'meta_query' => array(
            array(
                'key' => 'data_inicio',
                'compare' => '<=',
                'value' => $today
            ),
            array(
                'key' => 'data_final',
                'compare' => '>=',
                'value' => $today
            ),
            'relation' => 'AND'
        )
    );

    $projetosAndamento = new WP_Query($argsInicial);
    $projetosAndamentoJSON = array();
    $projeto = array();
    $autoresProjeto = array();
    $autor = array();
    while ($projetosAndamento->have_posts()) {
        $projetosAndamento->the_post();
        $projeto['titulo'] = get_the_title();
        $projeto['imagem_projeto'] = get_field('feature_img');
        $projeto['data_final'] = get_field('data_final'); //M, d Y
        $projeto['resumo'] = get_the_excerpt();
        $projeto['permalink'] = get_the_permalink();
        if (have_rows('orientadores')) {
            while (have_rows('orientadores')) {
                the_row();
                $idAutor = get_sub_field('orientador');
                $user = get_user_by('id', $idAutor);
                $autor['nome'] = $user->display_name;
                $autor['imagem_autor'] = get_avatar_url($user->ID);
                $autor['id'] = $idAutor;
                array_push($autoresProjeto, $autor);
            }
        } else {
            $autoresProjeto[0]['nome'] = get_the_author_meta('display_name', $post->post_author);
            $autoresProjeto[0]['id']= $post->post_author;
            $autoresProjeto[0]['imagem_autor'] = get_avatar_url($post->post_author);
        }
        $autor = array();
        $projeto['autor'] = $autoresProjeto[0];
        $autoresProjeto = array();
        array_push($projetosAndamentoJSON, $projeto);
    }

    return $projetosAndamentoJSON;
}

add_action('rest_api_init', function () {
    register_rest_route('api-portalbio/v1', '/ongoing-post/', array(
        'methods' => 'GET',
        'callback' => 'get_json_ongoing_posts',
    ));
});

function get_users_json($request)
{
    $type = $request['type'];
    $paged = ($request['paged']) ? $request['paged'] : 1;
    $search = ($request['search']) ? $request['search'] : "";
    switch ($type) {
        case 'alunos':
            $type = 'contributor';
            break;

        case 'professores':
            $type = 'editor';
            break;

        default:

            die();
    }

    $argsInicial = array(
        'role'    => $type,
        
        'order'   => 'ASC',
        'number' => 6,
        'paged' => $paged,
        'meta_key' => 'first_name',
        'orderby'  => 'meta_value',
        'meta_query' => array(
            'relation' => 'OR',
              array(
                'key' => 'first_name',
                'value' => $search,
                'compare' => 'LIKE'
              ),
            array(
                'key' => 'last_name',
                'value' => $search,
                'compare' => 'LIKE'
              )
            )
          
        // 'search' => "*$search*"
    );
    $usersInicial = get_users($argsInicial);
    $usuariosBusca = array();
    $usuariosInicial = array();
    $usuariosTemp = array();
    foreach ($usersInicial as $user) {
        $first_name = get_user_meta( $user->ID, 'first_name', true );
        $last_name = get_user_meta( $user->ID, 'last_name', true );
        $usuariosInicial['link'] = $user->user_nicename;
        $usuariosInicial['nome'] = $first_name." ".$last_name;
        $usuariosInicial['email'] = $user->user_email;
        $usuariosInicial['telephone'] = get_user_meta($user->ID, 'telefone', true);
        $usuariosInicial['ex_aluno'] = get_user_meta($user->ID, 'ex_aluno', true);
        $usuariosInicial['instituto'] = get_user_meta($user->ID, 'instituto', true);
        $usuariosInicial['img_url'] = get_avatar_url($user->ID);
        $usuariosInicial['registro_aluno'] = get_user_meta($user->ID, 'registro-aluno', true);
        array_push($usuariosTemp, $usuariosInicial);
    }
    return $usuariosTemp;
}
add_action('rest_api_init', function () {
    register_rest_route('api-portalbio/v1', '/users/(?P<type>[-\w]+)?(?:\/(?P<paged>\d+))(?:\/(?P<search>[-\w]+))?', array(
        'methods' => 'GET',
        'callback' => 'get_users_json',

    ));
});

add_action('rest_api_init', 'custom_api_get_all_posts');

function custom_api_get_all_posts()
{
    register_rest_route('api-portalbio/v1', '/all-posts', array(
        'methods' => 'GET',
        'callback' => 'custom_api_get_all_posts_callback'
    ));
}

function custom_api_get_all_posts_callback($request)
{
    // Initialize the array that will receive the posts' data. 
    $posts_data = array();
    $autoresProjeto = array();
    $autor = array();
    $final_date;
    // Receive and set the page parameter from the $request for pagination purposes
    $paged = intval($request->get_param('paged'));
    $type = $request->get_param('type');
    if ($type == 'any') {
        $type = array(
            'post',
            'artigos',
            'projetos'
        );
    }
    $paged = (isset($paged) || !(empty($paged))) ? $paged : 1;
    // Get the posts using the 'post' and 'news' post types
    $allPosts = new WP_Query(
        array(
            'paged' => $paged,
            'posts_per_page' => 3,
            'post_type' => $type // This is the line that allows to fetch multiple post types. 
        )
    );

    // Loop through the posts and push the desired data to the array we've initialized earlier in the form of an object
    while ($allPosts->have_posts()) {
        $allPosts->the_post();
        $id = get_the_ID();
        $post_thumbnail = (get_field('feature_img', $id)) ? get_field('feature_img', $id) : null;
        if (have_rows('orientadores', $id)) {
            while (have_rows('orientadores', $id)) {
                the_row();
                $idAutor = get_sub_field('orientador');
                $user = get_user_by('id', $idAutor);
                $autor['nome'] = $user->display_name;
                $autor['imagem_autor'] = get_avatar_url($user->ID);
                array_push($autoresProjeto, $autor);
            }
        } else {
            $autoresProjeto[0]['nome'] = get_the_author_meta('display_name', $post->post_author);
            $autoresProjeto[0]['imagem_autor'] = get_avatar_url($post->post_author);
        }
        if (get_field('data_inicial', $id)) {
            $final_date = date_create(get_field('data_inicial', $id));
            $final_date = date_format($final_date, 'M, d Y');
        } else {
            $final_date = get_post_time('M, d Y', false, $id); //date('M, d Y',)
        }
        $category_name = wpse_get_categories_names($id);
        $posts_data[] = (object) array(
            'id' => $id,
            'type' => $category_name,
            'title' => get_the_title(),
            'excerpt' => get_the_excerpt($id),
            'featured_img_src' => $post_thumbnail,
            'link' => get_the_permalink($id),
            'comments_number' => intval(get_comments_number($id)),
            'post_views_count' => getPostViews($id),
            'author' => $autoresProjeto[0],
            'post_date' => $final_date,
            'pagina' => $paged
        );
    }

    $response = new WP_REST_Response($posts_data, 200);

    // $response->header( 'X-WP-Total', $total ); // total = total number of post
    $response->header('X-WP-TotalPages', $allPosts->max_num_pages); // maximum number of pages
    wp_reset_postdata();
    return $response;
}
register_rest_field(
    array('post', 'artigos', 'projetos'),
    'views',
    array(
        'get_callback' => 'slug_get_post_views_count',
        'update_callback' => null,
        'schema' => null,
    )
);

function slug_get_post_views_count($post, $field_name, $request)
{
    return getPostViews($post['id']);
}


// add_action( 'rest_api_init', 'wpse_287931_register_categories_names_field' );

function wpse_get_categories_names($post)
{

    $formatted_categories = array();

    $categories = get_the_terms($post['id'], 'categorias');

    foreach ($categories as $category) {
        $formatted_categories[] = $category->name;
    }

    return $formatted_categories;
}



register_rest_field(
    array('post', 'artigos', 'projetos'),
    'categories_names',
    array(
        'get_callback'    => 'wpse_get_categories_names',
        'update_callback' => null,
        'schema'          => null,
    )
);

function get_recursos($key = '', $type = 'post', $status = 'publish')
{

    global $wpdb;

    if (empty($key))
        return;
    $totalCaptado = 0;
    $recursos = $wpdb->get_col($wpdb->prepare("
                SELECT pm.meta_value FROM {$wpdb->postmeta} pm
                LEFT JOIN {$wpdb->posts} p ON p.ID = pm.post_id
                WHERE pm.meta_key = %s 
                AND p.post_status = %s 
                AND p.post_type = %s
            ", $key, $status, $type));

    foreach ($recursos as $recurso) {
        $totalCaptado += intval($recurso);
    }

    return formatNumber(strval($totalCaptado));
}
function formatNumber($number = '')
{
    switch (strlen($number)) {
        default:
            return '<span class="numero">' . intval($number) . '</span>';
            break;

        case 5:
            $num = str_split($number, 1);
            $num = $num[0] . $num[1] . ($num[2] > 0 ? ',' . $num[2] : null);
            return '<span>+</span><span class="numero">' . intval($num) . '</span><span>mil</span>';
            break;
        case 6:
            $num = str_split($number, 1);
            $num = $num[0] . $num[1] . $num[2] . ($num[3] > 0 ? ',' . $num[3] : null);
            return '<span>+</span><span class="numero">' . intval($num) . '</span><span>mil</span>';
            break;
        case 7:
            $num = str_split($number, 1);
            $num = $num[0] . ($num[1] > 0 ? ',' . $num[1] : null);
            return '<span>+</span><span class="numero">' . intval($num) . '</span><span>mi</span>';
            break;
        case 8:
            $num = str_split($number, 1);
            $num = $num[0] . $num[1] . ($num[2] > 0 ? ',' . $num[2] : null);
            return '<span>+</span><span class="numero">' . intval($num) . '</span><span>mi</span>';
            break;
    }
}
function get_projetos_concluidos()
{
    $today = date('Ymd');
    $args = array(
        'post_type' => 'projetos',
        'posts_per_page' => -1,
        'meta_query' => array(
            array(
                'key' => 'data_final',
                'compare' => '>=',
                'value' => $today
            )
        )
    );
    $projetosConcluidos = new WP_Query($args);

    return $projetosConcluidos->found_posts;
}
function get_data_charts()
{
    $data = array();
    $today = date('Ymd');
    $argsAndamento = array(
        'post_type' => 'projetos',
        'posts_per_page' => -1,
        'meta_query' => array(
            array(
                'key' => 'data_inicio',
                'compare' => '<=',
                'value' => $today
            )
        )
    );
    $projetosAndamento = new WP_Query($argsAndamento);

    $data['pie'] = array();
    array_push($data['pie'], get_projetos_concluidos());
    array_push($data['pie'], $projetosAndamento->found_posts);


    return $data;
}
function custom_api_get_data_charts()
{
    register_rest_route('api-portalbio/v1', '/charts', array(
        'methods' => 'GET',
        'callback' => 'get_data_charts'
    ));
}
add_action('rest_api_init', 'custom_api_get_data_charts');

register_rest_field(
    array('post', 'artigos', 'projetos'),
    'categories_names',
    array(
        'get_callback'    => 'wpse_get_categories_names',
        'update_callback' => null,
        'schema'          => null,
    )
);


if(is_page('login') && is_user_logged_in()){



    
    $url = get_home_url() . "/perfil";

                  header("Location:" .   $url);

                            die();


         }

?>