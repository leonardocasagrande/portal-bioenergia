<?php get_header(); 

if ( $user->exists() ) { // is_user_logged_in() is a wrapper for this line

    $userdata = get_user_meta( $user->data->ID );
    
}
setPostViews(get_the_ID());
?> <div class="header-mask"></div><section><div class="container"><div class="col-12 text-center mb-5"><h1> <?php the_title(); ?> </h1></div><div class="row flex-wrap-reverse flex-md-wrap"><div class="col-md-4">orientadores <?php linked_users('orientadores', 'orientador', 'linked-users.php', $current_id, true); ?> EQUIPE <?php linked_users('equipe', 'aluno', 'linked-users.php', $current_id, true);  ?> APOIADORES <?php linked_users('apoiadores', 'apoiador', 'linked-supporters.php', $current_id, false);  ?> </div><div class="col-md-8"><div class="img-post" style="background: url('<?= get_the_post_thumbnail_url() ?>') center center no-repeat"></div><p> <?= get_the_content(null, null, $current_id); ?> </p><p></p><div class="d-flex"><span class="color-green mr-2">Vigência: </span> <?= the_field('data_inicio'); ?> à <?= the_field('data_final'); ?> </div><p></p><p></p><div class="d-flex"><span class="color-green">Valor do projeto:</span><div class="value ml-2"> <?= the_field('valor_do_projeto'); ?> </div></div><p></p></div></div><div class="col-md-9 m-auto video-single"> <?php

            if (get_field('is_externalvideo')) { ?> <video class="w-100" height="340" controls><source src="<?= $video = get_field('video')['url'] ?>" type="video/mp4"><source src="movie.ogg" type="video/ogg">Your browser does not support the video tag.</video> <?php

            } else {

                $video = the_field('youtube');

                echo $video;
            }

            ?> </div></div></section> <?php

include "section-apoie-projeto.php";

get_footer();


?>