<?php

global $header_type;

$btn_indicators = "white";

$nav_type = "white";

$bg_type = "transparent";

if ($header_type) {

  $nav_type = "light";

  $bg_type = "white";
}
?> <!doctype html><html lang="pt-br"><head><!-- Global site tag (gtag.js) - Google Analytics --><script async src="https://www.googletagmanager.com/gtag/js?id=UA-164289429-1"></script><script>window.dataLayer = window.dataLayer || [];

    function gtag() {
      dataLayer.push(arguments);
    }
    gtag('js', new Date());

    gtag('config', 'UA-164289429-1');</script><meta charset="utf-8"><meta name="viewport" content="width=device-width,initial-scale=1,shrink-to-fit=no"><meta name="description" content=""><meta name="author" content=""><link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/tiny-slider/2.9.2/tiny-slider.css"><!--[if (lt IE 9)]><script src="https://cdnjs.cloudflare.com/ajax/libs/tiny-slider/2.9.2/min/tiny-slider.helper.ie8.js"></script><![endif]--><link rel="stylesheet" type="text/css" href="<?= get_stylesheet_directory_uri(); ?>/dist/css/style.css"><title>Portal de Bioenergia</title> <?php wp_head(); ?> </head><body><header id="heading-s" class="position-fixed heading <?php if (!is_page(array('home', 'nossos-indicadores', 'doutorado'))){ echo 'color-gray-light';}elseif(is_page('home')){echo 'color-green';} ?>"><div class="container pt-1"><div class="d-flex align-items-center position-relative"><div class="col-lg-2"><a class="navbar-brand" href="<?= get_site_url() ?>" title="Portal de Bioenergia"><img class="logo" src="<?= get_stylesheet_directory_uri(); ?>/dist/img/logo-bio-energia.png" alt="Logo Portal de Bioenergia" title="Portal de Bioenergia"><br><img class="logo logo-unvs" src="<?= get_stylesheet_directory_uri(); ?>/dist/img/usp-unicamp-unesp.png" alt="Logo Portal de Bioenergia" title="Portal de Bioenergia"></a></div><div class="col-md-10 d-flex justify-content-end align-items-center" id="sub-menu"><div id="google_translate_element" class="d-none d-md-block"></div><div class="box"><a class="nav-link d-none d-md-block" href="<?= get_site_url() ?>/doutorado">O Curso</a></div><div class="box"><a class="nav-link d-none d-md-block" href="<?= get_site_url() ?>/professores">Professores</a></div><div class="box"><a class="nav-link d-none d-md-block" href="<?= get_site_url() ?>/alunos">Alunos</a></div><div class="ddd"><input type="checkbox" id="check"> <label id="btn" for="check" class="btn-menu mt-0 w-100"><div id="top"></div><div id="middle"></div><div id="bottom"></div></label><nav><div class="d-flex flex-wrap items-menu pl-0 text-center w-100 align-items-center"> <?php if (!is_user_logged_in()) { ?> <div><a href="<?= get_site_url() ?>/login"><i class="fas fa-sign-in-alt"></i> Log in</a></div> <?php } else { ?> <div><a href="<?= wp_logout_url() ?>"><i class="fas fa-sign-out-alt"></i> Sair</a></div><div> <?php

                    global $current_user;
                    $url_avatar = get_avatar_url($current_user->user_email);

                    ?> <a class="px-0" href="<?= get_home_url() ?>/perfil"><div class="avatar-menu" style="background: url(<?= fixAvatar($url_avatar) ?>) center center no-repeat"></div><span class="w-100">Perfil</span></a></div> <?php } ?> <div><a href="<?= get_home_url() ?>/doutorado"><i class="fas fa-graduation-cap"></i> Informações Doutorado</a></div><div><a href="<?= get_home_url() ?>/cadastro-alunos"><i class="fas fa-pen"></i> Cadastro Alunos</a></div><div><a href="<?= get_home_url() ?>/orientadores"><i class="fas fa-book-reader"></i> Cadastro<br>Orientadores</a></div><div><a href="<?= get_home_url() ?>/casos-de-sucesso"><i class="fas fa-check"></i> Casos de<br>Sucesso</a></div><div><a href="<?= get_home_url() ?>/parceiros"><i class="fas fa-handshake"></i> Parceiros</a></div><!-- <div>

                  <a href="<?= get_home_url() ?>/nossos-indicadores">
                    <i class="fas fa-chart-line"></i>
                    Indicadores

                  </a> --></div><div><a href="<?= get_home_url() ?>/contato"><i class="fas fa-address-book"></i> Contato</a></div><div><a href="<?= get_home_url() ?>/professores"><i class="fas fa-chalkboard-teacher"></i> Professores</a></div></nav></div><nav></nav></div></div></div></header><!--   <div id="box">

    <div id="items" class="d-flex flex-wrap justify-content-center">

      <div class="w-100">

        <div class="m-auto col-8">

          <hr>

        </div>

      </div>



    </div>

  </div> --></body></html>