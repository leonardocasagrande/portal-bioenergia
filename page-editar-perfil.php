<?php 

get_header();

?> <div class="header-mask"></div><div class="container mt-5 pt-5"> <?php 
$user = wp_get_current_user();

$allowed_roles = array('contributor');

if (array_intersect($allowed_roles, $user->roles)) {

  echo do_shortcode('[wpuf_profile type="profile" id="605"]');

}else{

  echo do_shortcode('[wpuf_profile type="profile" id="616"]');
  
}

?> </div> <?php 

get_footer();


?>