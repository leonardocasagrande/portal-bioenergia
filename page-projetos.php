<?php

global $header_type;
//0 = White | 1 = Colored
$header_type = 1;

?> <?php get_header(); ?> <section class="projetos-body"><div class="container"><div class="row mb-5"><div class="col-md-12 text-center color-black-light"><h1><? the_title()?></h1></div></div><div class="row" id="app-projetos"></div><div class="load-gif d-none text-center"><img class="spin mt-4" src="<?= get_stylesheet_directory_uri(); ?>/dist/img/symbol.png"></div><div class="text-center mt-4"><a class="load-more py-2 bg-green rounded text-white px-4 py-1" data-page="1" data-load="loadAny" data-post-type="projetos" id="load-projetos">Carregar mais</a></div></div></section> <?php get_footer(); ?>