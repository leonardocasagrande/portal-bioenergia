<?php get_header();

$current_id = get_the_ID();
setPostViews(get_the_ID());
// var_dump(get_post_meta(get_the_ID()));
?> <div class="header-mask"></div><section class="single-artigos"><div class="container"><div class="col-10 m-auto text-center py-5"><h1> <?php the_title(); ?> </h1></div><div class="row flex-wrap-reverse flex-md-wrap"><div class="col-12 mb-4"><div class="img-post" style="background: url('<?= the_field('feature_img') ?>') center center no-repeat"></div></div><div class="col-md-4"><div class="bg-concrete sidebar"><h3>Autor <?php echo get_the_author_meta('first_name') ?> <?php echo get_the_author_meta('last_name') ?> </h3></div><div class="download"><a target="_blank" href="<?= the_field('download') ?>"><i class="fas fa-download color-green"></i> Download</a><hr><span><i class="fas fa-eye color-green"></i> <?php echo getPostViews(get_the_ID()) ?></span></div></div><div class="col-md-8"><p> <?= get_the_content(null, null, $current_id); ?> </p><!-- <?php 
        $tags= get_the_tags($current_id);
        ?> <span class="color-green">Tags:</span> <?php
        foreach($tags as $tag){
            echo "<span class='tag'>$tag->name</span>";
        }
        ?> --></div></div><div class="row"><div class="col-12 col-md-8 mt-2 m-auto"><h3 class="mb-3 text-center">Comentários</h3> <?php comments_template(); ?> </div></div></div></section> <?php get_footer(); ?>