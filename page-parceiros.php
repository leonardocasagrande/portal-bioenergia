<?php

global $header_type;
//0 = White | 1 = Colored
$header_type = 1;

$user = wp_get_current_user();

if ($user->exists()) { // is_user_logged_in() is a wrapper for this line

    $userdata = get_user_meta($user->data->ID);
}

?> <?php get_header(); ?> <section class="parceiros-body"><div class="container"><div class="row mb-5"><div class="col-md-12 text-center color-black-light"><h1>Parceiros</h1>O Programa Integrado de Pós-graduação em Bioenergia possui e realiza parcerias com instituições públicas e privadas para apoio financeiro e institucional aos projetos em andamento. Conheça alguns desses parceiros. Quer se tornar um parceiro? Veja como em Seja um Parceiro (link para regras de como se tornar um parceiro). Não tenho esse documento. Vou ver com o Professor.</div></div><div class="row pt-5"> <?php
            $loop = new WP_Query(array('post_type' => 'apoiadores', 'orderby' => 'title', 'order' => 'ASC'));
            if ($loop->have_posts()) :
                while ($loop->have_posts()) : $loop->the_post(); ?> <div class="col-md-6 my-5"><div class="row mx-2 item"><div class="col-md-12 mb-5"><img src="<?= get_field("logomarca") ?>" alt="Logo <?= get_the_title(); ?>" title="<?= get_the_title(); ?>"></div><div class="col-md-12 mt-5"><div class="row"><div class="col-md-6 pl-4"> <?= get_field("descricao_curta") ?> </div><div class="col-md-6 valor"><!--                              <span class="text-center text-md-left">+23mi</span>

                                        <small class="text-center text-md-left">de recursos doados</small> --></div><div class="col-md-12 text-center py-5"><a class="btn btn-transparent" href="<?php the_permalink(); ?>" title="Perfil da <?= get_the_title(); ?>">Saiba mais</a></div></div></div></div></div> <?php endwhile;
                if ($loop->max_num_pages > 1) : ?> <div id="nav-below" class="navigation"><div class="nav-previous"><?php next_posts_link(__('<span class="meta-nav">&larr;</span> Previous', 'domain')); ?></div><div class="nav-next"><?php previous_posts_link(__('Next <span class="meta-nav">&rarr;</span>', 'domain')); ?></div></div> <?php endif;
            endif;
            wp_reset_postdata();
            ?> </div></div></section> <?php

include "section-apoie-projeto.php";

get_footer();
?>