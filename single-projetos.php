<?php get_header(); setPostViews(get_the_ID()); the_post();?> <div class="header-mask"></div><section class="single-projetos"><div class="container"><div class="col-10 m-auto text-center py-5"><h1> <?php the_title(); ?> </h1></div><div class="row flex-wrap-reverse flex-md-wrap align-items-start"><div class="col-md-4 bg-concrete sidebar"><h3>orientadores</h3> <?php linked_users('orientadores', 'orientador', 'linked-users.php', $current_id, true); ?> <hr><h3>EQUIPE</h3> <?php linked_users('equipe', 'aluno', 'linked-users.php', $current_id, true);  ?> <hr><h3>APOIADORES</h3> <?php linked_users('apoiadores', 'apoiador', 'linked-supporters.php', $current_id, false);  ?> <?php $idOrientador=0;
                $orientadores = get_post_meta( get_the_ID(), 'orientadores_wp' );
                $idOrientador = $orientadores[0];
                ?> <a href="<?= get_home_url() ?>/contato?authorId=<?= $idOrientador ?>" class="btn-cta mt-4">Apoiar</a></div><div class="col-md-8"><div class="img-post" style="background: url('<?= get_field('feature_img') ?>') center center no-repeat"></div><p class="mt-4"> <?= get_the_content(null, null, $current_id); ?> </p><p></p><div class="d-flex"><b class="color-green mr-2">Vigência: </b> <?= the_field('data_inicio'); ?> <?php if(get_field('data_final')):?> à <?= the_field('data_final'); ?> <?php endif;?> </div><p></p><p></p><div class="d-flex"><b class="color-green">Valor do projeto:</b><div class="value ml-2"> <?= the_field('valor_do_projeto'); ?> </div></div><p></p></div></div><div class="row justify-content-center"><div class="col-md-9 my-4 video-single"> <?php

if (get_field('is_externalvideo')) { ?> <video class="w-100" height="340" controls><source src="<?= $video = get_field('video')['url'] ?>" type="video/mp4"><source src="movie.ogg" type="video/ogg">Your browser does not support the video tag.</video> <?php

} else {

    $video = the_field('youtube');

    echo $video;
}

?> </div></div></div><div class="bg-concrete separador"></div><div class="container indicadores-single-projeto contador p-4"><h2 class="color-green text-center mb-4">Indicadores do projeto</h2><div class="row text-md-center"><div class="col-md-6"><div class="number"><p class="titulo mb-1 color-green">Patentes</p><span class="numero"><?php echo get_field('patentes') ?></span></div></div><div class="col-md-6"><div class="number"><p class="titulo mb-1 color-green">Recursos captados</p> <?php echo get_recursos('recursos_capitados','projetos') ?> </div></div></div></div></section> <?php

include "section-apoie-projeto.php";

get_footer();


?>